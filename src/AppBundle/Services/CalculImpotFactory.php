<?php 
namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use  Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entreprise;

/**
* factory service to return the impot value
* based in ca and entreprise type wich now used in static mode
*
*/
class CalculImpotFactory
{
	/**
    * calcul impot value
    * @param string $type
    * @param float $ca
    *@return float | string
    */
    function getImpot($type, $ca)
    {
       if($ca != null) {

        switch ($type) {
            case 'auto':
                $taux = 0.25;
                break;
            case 'sas':
                $taux = 0.33;
                break;
            default:
                return false;
                break;
        }
        return $ca*$taux;
       }
    }
}