<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Entreprise controller.
 *
 * @Route("entreprise")
 */
class EntrepriseController extends Controller
{
    static AUTO = 'auto';
    static SAS = 'sas';

    /**
     * return impot with static values in input
     *
     * @Route("/", name="entreprise_index_no_params")
     * @Route("/{type}/{ca}", name="entreprise_index")
     * @Method("GET")
     */
    public function indexAction($type = null ,  $ca = null, Request $request)
    {   
        $request->attributes->set('ca', 12000);
        $request->attributes->set('type', self::AUTO);

        $ca  = ($ca === null)?$request->attributes->get('ca'):$ca;
        $type = ($type === null)?$request->attributes->get('type'):$type;
        $service = $this->get('calcul.factory');
        $impot = $service->getImpot($type, $ca);
        if($impot === false) {
            throw new NotFoundHttpException("Merci de vérifier vos entré, cas pas encore traité");
        }
        return $this->render('AppBundle:entreprise:index.html.twig', array(
            'impot' => $impot,
            'type' => $type,
            'ca' => $ca
        ));
    }
}
