<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entreprise
 *
 * @ORM\Table(name="entreprise")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sirret", type="string", length=255)
     */
    private $sirret;

    /**
     * @var string
     *
     * @ORM\Column(name="denomination", type="string", length=255)
     */
    private $denomination;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="text", nullable=true)
     */
    private $adresse;


    /**
    * relation one to one avec lentity image
    * @ORM\OneToOne(targetEntity="AppBundle\Entity\EntrepriseType")
    * @ORM\JoinColumn(nullable=true)
    */
    protected $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sirret
     *
     * @param string $sirret
     *
     * @return Entreprise
     */
    public function setSirret($sirret)
    {
        $this->sirret = $sirret;

        return $this;
    }

    /**
     * Get sirret
     *
     * @return string
     */
    public function getSirret()
    {
        return $this->sirret;
    }

    /**
     * Set denomination
     *
     * @param string $denomination
     *
     * @return Entreprise
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Get denomination
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Entreprise
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\EntrepriseType $type
     *
     * @return Entreprise
     */
    public function setType(\AppBundle\Entity\EntrepriseType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\EntrepriseType
     */
    public function getType()
    {
        return $this->type;
    }
}
