<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntrepriseType response entity
 *
 * @ORM\Table(name="entreprise_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntrepriseTypeRepository")
 */
class EntrepriseType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="impot", type="float")
     */
    private $impot;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EntrepriseType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set impot
     *
     * @param \double $impot
     *
     * @return EntrepriseType
     */
    public function setImpot(\double $impot)
    {
        $this->impot = $impot;

        return $this;
    }

    /**
     * Get impot
     *
     * @return \double
     */
    public function getImpot()
    {
        return $this->impot;
    }
}
